function PC_start_screen() {

  var show_start_button = true;
  var input;

  this.setup = function() {
    input = createInput();
    input.position(width / 2 - 100, height/4);
    input.value(8);

    socket.on('new_serveur', function(new_server) {
      console.log(new_server.number);
      sceneManager.serveur_id = new_server.number;
      sceneManager.showScene(PC_waiting_room);
    });

  }

  this.draw = function() {
    background(210);
    textSize(15);
    fill(0);
    text('Nombre de joueurs maximum : (max 8)', width / 2 - 150, height/4 - 20);
    fill(255);
    rectMode(CENTER);
    rect(width/2,height/4 * 3,500,100);
    textSize(50);
    fill(0);
    text('Créer un serveur',width/2 - 200,height/4 * 3 + 20);
    textSize(0.15 * width);
    textStyle(BOLD);
    textFont('Helvetica');
    fill(120);
    text('Stickit.io', width/2 - 250, height / 2);
    textSize(40);
    fill(180);
    textStyle(ITALIC);
    text('Rejoignez la partie sur', width/2 - 220, height / 2 + 70);
    text('stickit.deltabooq.ch', width/2 - 200, height / 2 + 140);

    if(mouseIsPressed && mouseX > width/2 - 250 && mouseX < width/2 + 250 && mouseY > height/4 * 3 - 50 && mouseY < height/4 * 3 + 50 && show_start_button) {
      if(input.value() <= 8 && input.value() >= 2) {
        sceneManager.max_joueurs = input.value();
        this.sceneManager.socket.emit('new_serveur', {
          max_joueurs: input.value()
        });
        input.remove();
        show_start_button = false;
      }
    }
  }



}
