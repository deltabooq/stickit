function createJoueur(id, color, joueurs, RATIO_SCREEN) {
  joueur = createSprite(random(50, width - 50), 250, 0, 0);
  joueur.addAnimation('standingleft', '../images/player/standingleft/SL.png');
  joueur.addAnimation('standingright', '../images/player/standingright/SR.png');
  joueur.addAnimation('runningleft', '../images/player/runningleft/L1.png', '../images/player/runningleft/L4.png');
  joueur.addAnimation('runningright', '../images/player/runningright/R1.png', '../images/player/runningright/R4.png');
  joueur.addAnimation('jumpingleft', '../images/player/jumpingleft/L1.png', '../images/player/jumpingleft/L13.png');
  joueur.addAnimation('jumpingright', '../images/player/jumpingright/R1.png', '../images/player/jumpingright/R13.png');
  joueur.addAnimation('holdingleft', '../images/player/holdingleft/HL.png');
  joueur.addAnimation('holdingright', '../images/player/holdingright/HR.png');
  /*joueur.scale = 0.25 * RATIO_SCREEN;
  joueur.setCollider('rectangle', 0, -5, 250 * RATIO_SCREEN , 3500 * RATIO_SCREEN);*/
  joueur.scale = 0.3;
  joueur.setCollider('rectangle', 0, -5, 120, 250);
  joueur.id = id;
  joueur.vie = 10;
  joueur.jump_left = 0;
  joueur.timer_bonus = 0;
  joueur.id_bonus = 0;
  joueur.last_acceleration_y = 0;
  joueur.couleur = color;
  joueur.last_side_looked = true; //true = droite / false = gauche
  joueurs.add(joueur);
}

function draw_joueurs(joueurs, GRAVITY, AMOUNT_JUMP, plateformes_top, plateformes_sides, plateformes_bottom, tirs) {


  joueurs.collide(plateformes_sides);
  joueurs.collide(plateformes_top);

  for(var i = 0; i < joueurs.length; i++) {


    joueurs[i].velocity.x = joueurs[i].last_acceleration_y;
    joueurs[i].velocity.y += GRAVITY;
    if(joueurs[i].overlap(plateformes_top)) {
      joueurs[i].velocity.y = 0;
      joueurs[i].jump_left = AMOUNT_JUMP;
    }

    if(joueurs[i].overlap(plateformes_bottom)) {
      joueurs[i].velocity.y = abs(joueurs[i].velocity.y) / 2;
    }

    //a déplacer dans tirs.js
    for(var j = 0; j < tirs.length; j++) {
      if(tirs[j].position.x > joueurs[i].position.x - 18 && tirs[j].position.x < joueurs[i].position.x + 18 && tirs[j].position.y > joueurs[i].position.y - 42 && tirs[j].position.y < joueurs[i].position.y + 32.5 && joueurs[i].couleur != tirs[j].shapeColor) {
        joueurs[i].vie--;
        tirs[j].remove();
      }
    }

    //bonus
    if(joueurs[i].timer_bonus > 0) {
      joueurs[i].timer_bonus--;
    }else{
      joueurs[i].id_bonus = 0;
    }

    switch (joueurs[i].id_bonus) {
      case 1:
      createTir(joueurs[i].position.x, joueurs[i].position.y, joueurs[i].last_side_looked, joueurs[i].couleur, tirs);
      break;
    }


    //Animations
    if(joueurs[i].last_acceleration_y > 0) {
      joueurs[i].changeAnimation('runningright');
      joueurs[i].last_side_looked = true;
    }else if(joueurs[i].last_acceleration_y < 0) {
      joueurs[i].changeAnimation('runningleft');
      joueurs[i].last_side_looked = false;
    }else if(joueurs[i].last_side_looked) {
      joueurs[i].changeAnimation('standingright')
    }else {
      joueurs[i].changeAnimation('standingleft')
    }

    //Couleur + affichage
    tint(joueurs[i].couleur);

    rectMode(CORNER);
    fill(joueurs[i].couleur);
    rect(joueurs[i].position.x - 20, joueurs[i].position.y - 80, 100, 10);
    fill(255);
    rect((joueurs[i].position.x - 20) + (joueurs[i].vie * 10), joueurs[i].position.y - 80, 100 - (joueurs[i].vie * 10), 10);
    drawSprite(joueurs[i]);

    fill(255);
    /*text(joueurs[i].jump_left, joueurs[i].position.x, joueurs[i].position.y - 50);
    text(joueurs[i].velocity.y, joueurs[i].position.x, joueurs[i].position.y - 60);
    text(joueurs[i].id, joueurs[i].position.x, joueurs[i].position.y - 70);*/
    //text(joueurs[i].vie, joueurs[i].position.x, joueurs[i].position.y - 60);
    text(joueurs[i].id_bonus, joueurs[i].position.x, joueurs[i].position.y - 60);
    text(joueurs[i].timer_bonus, joueurs[i].position.x, joueurs[i].position.y - 80);

    if(joueurs[i].position.y > height + 500) {
      joueurs[i].vie = 0;
    }

    //respawn
    if(joueurs[i].vie <= 0) {
      joueurs[i].position.x = random(50, width - 50);
      joueurs[i].position.y = -60;
      joueurs[i].velocity.y = 0;
      joueurs[i].jump_left = 0;
      joueurs[i].vie = 10;
    }

    //joueurs[i].debug = true;
  }
}
