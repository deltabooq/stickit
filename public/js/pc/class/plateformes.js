function createPlateforme(plateformes_sides, plateformes_top, plateformes_bottom) {
  var rand_width = random(width);
  var rand_height = random(height);
  var plateforme_top = createSprite(rand_width, rand_height - 2.6 , 295, 50);
  var plateforme_sides = createSprite(rand_width, rand_height, 300, 50);
  var plateforme_bottom = createSprite(rand_width, rand_height + 25 , 295, 5);
  plateforme_top.setCollider('rectangle');
  plateforme_sides.setCollider('rectangle');
  plateforme_bottom.setCollider('rectangle');
  plateforme_top.shapeColor = color(80);
  plateforme_sides.shapeColor = color(100);
  plateforme_bottom.shapeColor = color(70);
  plateformes_top.add(plateforme_top);
  plateformes_sides.add(plateforme_sides);
  plateformes_bottom.add(plateforme_bottom);
}

function draw_plateformes(plateformes_sides, plateformes_top, plateformes_bottom) {
    drawSprites(plateformes_sides);
    drawSprites(plateformes_top);
    drawSprites(plateformes_bottom);
}
