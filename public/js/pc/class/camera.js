function draw_camera(joueurs) {
  //Gestion de la caméra
  var moyenne_pos_x = 0;
  var moyenne_pos_y = 0;
  var moyenne_distance_pos_x = 0;
  var moyenne_distance_pos_y = 0;
  var distance_moyenne = 0;
  var goal_zoom_camera = 0;
  var goal_pos_x_camera = 0;
  var goal_pos_y_camera = 0;

  //Emplacement de la caméra
  for(var i = 0; i < joueurs.length; i++) {
    moyenne_pos_x += joueurs[i].position.x;
    moyenne_pos_y += joueurs[i].position.y;
  }
  moyenne_pos_x /= joueurs.length;
  moyenne_pos_y /= joueurs.length;
  goal_pos_x_camera = moyenne_pos_x;
  goal_pos_y_camera = moyenne_pos_y;
  camera.position.x += (goal_pos_x_camera - camera.position.x) / 10;
  camera.position.y += (goal_pos_y_camera - camera.position.y) / 10;

  //Zoom de la caméra
  for(var i = 0; i < joueurs.length; i++) {
    moyenne_distance_pos_x += abs(joueurs[i].position.x - moyenne_pos_x);
    moyenne_distance_pos_y += abs(joueurs[i].position.y - moyenne_pos_y);
  }
  moyenne_distance_pos_x /= joueurs.length;
  moyenne_distance_pos_y /= joueurs.length;
  distance_moyenne = sq(sqrt(moyenne_distance_pos_x) + sqrt(moyenne_distance_pos_y));
  goal_zoom_camera = (width / 1500) - (distance_moyenne / 4000);        //width / 1500 --> le zoom varie selon la taille de du jeu
  //console.log(goal_zoom_camera);
  camera.zoom += (goal_zoom_camera - camera.zoom) / 10;
}
