function set_sockets(joueurs, JUMP, tirs) {

  this.sceneManager.socket.on('acceleration', function(acc) {
    for(var i = 0; i < joueurs.length; i++) {
      if(acc.id_joueur == joueurs[i].id) {
        joueurs[i].last_acceleration_y = acc.y;
      }
    }
  });

  this.sceneManager.socket.on('jump', function(jump) {
    for(var i = 0; i < joueurs.length; i++) {
      if(jump.id_joueur == joueurs[i].id) {
        if(joueurs[i].jump_left > 0) {
          joueurs[i].velocity.y = -JUMP;
          joueurs[i].jump_left--;
        }
      }
    }
  });

  this.sceneManager.socket.on('tir', function(tir) {
    for(var i = 0; i < joueurs.length; i++) {
      if(tir.id_joueur == joueurs[i].id) {
        createTir(joueurs[i].position.x, joueurs[i].position.y, joueurs[i].last_side_looked, joueurs[i].couleur, tirs);
      }
    }
  });

  this.sceneManager.socket.on('delete_connected_joueur', function(joueur) {
    for(var i = 0; i < joueurs.length; i++) {
      if(joueurs[i].id == joueur.joueur_id) {
        joueurs[i].remove();
      }
    }
  });
}
