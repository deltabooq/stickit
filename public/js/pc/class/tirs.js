function createTir(x, y, direction, color, tirs) {
  tir = createSprite(x, y, 10, 10, tirs);
  tir.setCollider('rectangle', 0, 0, 10, 10);
  tir.direction = direction;
  tir.shapeColor = color;
  tir.couleur = color;
  tirs.add(tir);
  tir.life = 200;
}

function draw_tirs(tirs, plateformes_sides, TIRS_SPEED) {
  for(var i = 0; i < tirs.length; i++) {
    if(tirs[i].direction) {
      tirs[i].position.x += TIRS_SPEED;
    }else{
      tirs[i].position.x -= TIRS_SPEED;
    }
    //tirs[i].debug = true;
    if(tirs[i].overlap(plateformes_sides)) {
      tirs[i].remove();
    }
  }
  drawSprites(tirs);
}
