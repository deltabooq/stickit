function createBonus(bonuss, plateformes_top) {
  var x = random(50, width - 50);
  var y = random(50, height - 50);

  for(var i = 0; i < plateformes_top.length; i++) {
    while(plateformes_top[i].overlapPoint(x, y)) {
      x = random(50, width - 50);
      y = random(50, height - 50);
    }
  }

  var bonus = createSprite(random(width), random(height), 20, 20);
  bonus.setCollider('rectangle');
  bonus.shapeColor = color(255);
  bonuss.add(bonus);
}

function draw_bonus(bonuss, plateformes_sides, plateformes_top, GRAVITY, joueurs, time_respawn_bonus) {

  bonuss.collide(plateformes_sides);
  bonuss.collide(plateformes_top);




  for(var i = 0; i < bonuss.length; i++) {
    bonuss[i].velocity.y += GRAVITY;

    if(bonuss[i].overlap(plateformes_top)) {
      bonuss[i].velocity.y = 0;
    }

    for(var j = 0; j < joueurs.length; j++) {
      if(typeof bonuss[i] !== 'undefined' && bonuss[i].overlapPoint(joueurs[j].position.x, joueurs[j].position.y + 20)) {
        bonuss[i].remove();
        joueurs[j].timer_bonus = 300;
        joueurs[j].id_bonus = 1;
      }
    }
  }

  drawSprites(bonuss);
}
