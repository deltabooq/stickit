function PC_host() {

  var window_width = 0;
  var window_height = 0;

  var plateformes_sides;
  var plateformes_top;
  var plateformes_bottom;
  var joueurs;
  var tirs;
  var bonuss;

  var time_respawn_bonus = 0;
  var AMOUNT_JUMP = 3;
  var GRAVITY = 0.5;
  var JUMP = 12;
  var SPEED = 10;
  var TIRS_SPEED = 30;

  this.setup = function() {
    plateformes_sides = new Group();
    plateformes_top =new Group();
    plateformes_bottom =new Group();
    joueurs = new Group();
    tirs = new Group();
    bonuss = new Group();

    RATIO_SCREEN = width / 1920;

    //Création des éléments
    var ground = createSprite(width/2, height, width, 50);
    ground.setCollider('rectangle');
    ground.shapeColor = color(220);
    plateformes_top.add(ground);

    createPlateforme(plateformes_sides, plateformes_top, plateformes_bottom);
    createPlateforme(plateformes_sides, plateformes_top, plateformes_bottom);
    createPlateforme(plateformes_sides, plateformes_top, plateformes_bottom);
    createPlateforme(plateformes_sides, plateformes_top, plateformes_bottom);
    createPlateforme(plateformes_sides, plateformes_top, plateformes_bottom);
    createPlateforme(plateformes_sides, plateformes_top, plateformes_bottom);
    createPlateforme(plateformes_sides, plateformes_top, plateformes_bottom);
    createPlateforme(plateformes_sides, plateformes_top, plateformes_bottom);
    createPlateforme(plateformes_sides, plateformes_top, plateformes_bottom);

    for(var i = 0; i < sceneManager.connected_joueurs; i++) {
      createJoueur(sceneManager.joueurs_current_game[i].id, sceneManager.joueurs_current_game[i].color, joueurs, RATIO_SCREEN);
    }

    set_sockets(joueurs, JUMP, tirs);
  }

  window.onresize = function(event) {
    resizeCanvas(windowWidth, windowHeight, false);
  }

  this.draw = function() {

    //a déplacer dans bonus
    if(time_respawn_bonus > 0.0) {
      time_respawn_bonus--;
    }else{
      time_respawn_bonus = random(1800, 3000);
      createBonus(bonuss, plateformes_top);
    }

    background(60);

    fill(255);
    console.log(time_respawn_bonus);

    fill(232, 187, 39);
    ellipseMode(CENTER);
    ellipse(camera.position.x / 2, camera.position.y / 2, 100, 100);

    draw_joueurs(joueurs, GRAVITY, AMOUNT_JUMP, plateformes_top, plateformes_sides, plateformes_bottom, tirs);
    draw_tirs(tirs, plateformes_sides, TIRS_SPEED);
    draw_plateformes(plateformes_sides, plateformes_top, plateformes_bottom);
    draw_bonus(bonuss, plateformes_sides, plateformes_top, GRAVITY, joueurs, time_respawn_bonus);
    draw_camera(joueurs);

    //joueurs[0].position.x = mouseX * 2;
    //joueurs[0].position.y = mouseY * 2;
  }

  this.mousePressed = function() {
  }

}
