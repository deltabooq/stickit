function PC_waiting_room() {

  var show_start_button = true;
  var connected_joueurs = 0;

  this.setup = function() {
    socket.on('new_serveur', function(new_server) {
      console.log(new_server.number);
    });

    socket.on('new_connected_joueur', function(joueur) {
      if(joueur.serveur_id == sceneManager.serveur_id) {
        connected_joueurs++;
      }
    });

    socket.on('delete_connected_joueur', function(joueur) {
      if(joueur.serveur_id == sceneManager.serveur_id) {
        connected_joueurs--;
      }
    });

    socket.on('conf_start_game', function (game) {
      sceneManager.joueurs_current_game = game.joueurs;
      sceneManager.connected_joueurs = connected_joueurs;
      sceneManager.showScene(PC_host);
    });
  }

  this.draw = function() {
    background(210);
    fill(255);
    rectMode(CENTER);
    rect(width/2,height/2,500,100);
    textSize(50);
    fill(0);
    text('Code : ' + sceneManager.serveur_id, width/2 - 200, height/2 + 20);
    textSize(20);
    text('Nombre de joueurs connectés : ' + connected_joueurs + '/' + sceneManager.max_joueurs, width/2 - 200, height - 40);

    if(connected_joueurs >= 2) {
      fill(255);
      rect(width/2, height/2 - 150, 300, 100);
      textSize(50);
      fill(0);
      text('START', width/2 - 80, height/2 - 130);
      if(mouseIsPressed && mouseX > width/2 - 150 && mouseX < width/2 + 150 && mouseY > height/2 - 200 && mouseY < height/2 + 100) {
        socket.emit('start_game');
      }
    }

  }



}
