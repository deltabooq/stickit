function mobile_start_screen() {

  var input, button;

  this.setup = function() {
    background(210);
    input = createInput();
    input.position(20, height/2);

    button = createButton('submit');
    button.position(input.x + input.width + 10, height/2);
    button.mousePressed(connect_serveur);


    socket.on('accept_connect', function(joueur) {
      sceneManager.joueur_color = joueur.color;
      sceneManager.server_socket_id = joueur.server_socket_id;
      sceneManager.joueur_id = joueur.id;
      sceneManager.serveur_id = joueur.serveur_id;
      input.remove();
      button.remove();
      sceneManager.showScene(mobile_client);
    });

    socket.on('unknown_connect', function() {
      background(255, 168, 168);
    });

    socket.on('serveur_full', function() {
      alert('le serveur est plein');
    });

    socket.on('game_has_started', function() {
      alert('le jeu a déjà commencé sur ce serveur');
    });

  }

  this.draw = function() {
    if(sceneManager.can_connect) {
      sceneManager.showScene(mobile_client);
    }
  }

  function connect_serveur() {
    this.sceneManager.socket.emit('new_joueur', {
      serveur_id: input.value()
    });
  }

  function windowResized() {
    console.log('hey');
  }

}
