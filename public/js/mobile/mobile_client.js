function mobile_client() {

  var acceleration_x;
  var acceleration_y;

  this.draw = function() {
    background(sceneManager.joueur_color);
    fill(255);
    /*text('Id du joueur : ' + sceneManager.joueur_id, 20, height - 60);
    text('Id du serveur : ' + sceneManager.serveur_id, 20, height - 40);*/
    rectMode(CENTER);
    noStroke();
    rect(width/2,height/2,width,10);

    fill(0, 50);
    rotate(PI / 2);
    textSize(35);
    text('tirer', height / 4, -(width/2));
    text('sauter', height / 4 * 3, -(width/2));
  }

  this.setup = function() {
    socket.on('server_disconnected', function(serveur) {
      if(sceneManager.serveur_id == serveur.serveur_id) {
        console.log('serveur disconnected');
        alert('Le serveur a été déconnecté.');
        location.reload();
      }
    });
  }

  //accelerometer
  window.addEventListener ('devicemotion', function(e) {
    var old_acc_x = acceleration_x;
    var old_acc_y = acceleration_y;
    acceleration_x = parseInt(e.accelerationIncludingGravity.x);
    acceleration_y = parseInt(e.accelerationIncludingGravity.y);

    if(old_acc_y != acceleration_y) {
      if(sceneManager.device_OS == 'iOS') {
        acceleration_x = -acceleration_x;
        acceleration_y = -acceleration_y;
      }
      this.sceneManager.socket.emit('acceleration_mobile', {
        x: acceleration_x,
        y: acceleration_y,
        id_joueur: sceneManager.joueur_id,
        serveur_socket_id: sceneManager.server_socket_id
      });
    }

    textSize(25);
    text(acceleration_x + ' ' + acceleration_y, 20, 20);
  });


  this.touchStarted = function() {
    if(mouseY > 0 && mouseY < height / 2 - 5) {
      this.sceneManager.socket.emit('tir', {
        id_joueur: sceneManager.joueur_id,
        serveur_socket_id: sceneManager.server_socket_id
      });
    }
    if(mouseY > height / 2 - 5 && mouseY < height) {
      this.sceneManager.socket.emit('jump', {
        id_joueur: sceneManager.joueur_id,
        serveur_socket_id: sceneManager.server_socket_id
      });
    }
  }

  this.mousePressed = function() {
    var fs = fullscreen();
    if(!fs) {
      fullscreen(true);
    }
  }

}
