var socket = io();
var sceneManager;

window.onbeforeunload = function(e) {
  socket.disconnect();
};


function setup() {
  createCanvas(windowWidth,windowHeight);

  $('body').css('background-color', /*'#d2d2d2'*/'#ffffff');

  sceneManager = new SceneManager();
  sceneManager.socket = socket;
  sceneManager.wire();
  sceneManager.showScene(PC_start_screen);

  if(getMobileOperatingSystem() == 'iOS') {
    sceneManager.device_OS = 'iOS';
  }else{
    sceneManager.device_OS = 'Android';
  }

  if(windowWidth < 700) {
    resizeCanvas(windowWidth,windowHeight, true);
    sceneManager.showScene(mobile_start_screen);
  }else{
    $('canvas').css({
      'margin-left': (windowWidth - width) / 2,
      'margin-top': (windowHeight - height) / 2,
    });
  }
}


function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;
  // Windows Phone must come first because its UA also contains "Android"
  //if (/windows phone/i.test(userAgent)) {
  //  return "Windows Phone";
  //}
  if (/android/i.test(userAgent)) {
    return "Android";
  }
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return "iOS";
  }
  return "unknown";
}
