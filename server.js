var express = require('express');
var app = express();
var server = app.listen(3000);
app.use(express.static('public'));

console.log('Serveur running!');

var socket = require('socket.io');
var io = socket(server);

function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

var serveurs = {};
var couleurs = ['#e67e22', '#c0392b', '#95a5a6', '#f1c40f', '#3498db', '#2ecc71', '#9b59b6', '#1abc9c'];
shuffle(couleurs);

io.sockets.on('connection', function(socket) {
  //ce qu'il y a ici est propre à chaque utilisateur
  var serveur_me = {};
  var joueur_me = {};
  console.log('nouvel utilisateur');

  socket.on('new_serveur', function(attributs) {
    serveur_me.id = getRandomNumber(1000, 9999);
    while(serveur_me.id in serveurs) {
      serveur_me.id = getRandomNumber(1000, 9999);
    }

    serveur_me.max_joueurs = attributs.max_joueurs;
    serveur_me.connected_joueur = 0;
    serveur_me.game_started = false;
    serveur_me.list_joueurs = {};
    serveur_me.socket_id = socket.id;

    serveurs[serveur_me.id] = serveur_me;
    socket.emit('new_serveur', {
      number: serveur_me.id
    });


  });

  socket.on('new_joueur', function(new_joueur) {

    if(new_joueur.serveur_id in serveurs) {
      if(serveurs[new_joueur.serveur_id].connected_joueur < serveurs[new_joueur.serveur_id].max_joueurs) {
        if(!serveurs[new_joueur.serveur_id].game_started) {

          serveurs[new_joueur.serveur_id].connected_joueur ++;
          io.sockets.emit('new_connected_joueur', {
            serveur_id: new_joueur.serveur_id
          });

          joueur_me.id = getRandomNumber(100000, 999999);
          while(joueur_me.id in serveurs[new_joueur.serveur_id].list_joueurs) {
            joueur_me.id = getRandomNumber(100000, 999999);
          }

          //Choisir la couleur du nouveau joueur
          //joueur_me.color = couleurs[serveurs[new_joueur.serveur_id].list_joueurs.length];
          joueur_me.color = couleurs[0];
          couleurs.splice(0, 1);
          console.log(couleurs);
          joueur_me.serveur_id = new_joueur.serveur_id;
          joueur_me.socket_id = socket.id;
          joueur_me.server_socket_id = serveurs[new_joueur.serveur_id].socket_id;

          /*var check_color = true;
          while(check_color) {  //check si elle est prise
          var color_taken = false;
          for(var i in serveurs[new_joueur.serveur_id].list_joueurs) {
          if(joueur_me.color in serveurs[new_joueur.serveur_id].list_joueurs[i].color) {
          color_taken = true;
          //}
          //}
          if(color_taken) {
          joueur_me.color = flatColors(getRandomColor());
          //}else{
          check_color = false;
          //}
          //}*/
          //serveurs[new_joueur.serveur_id].list_joueurs[joueur_me.id].color = joueur_me.color;

          serveurs[new_joueur.serveur_id].list_joueurs[joueur_me.id] = joueur_me;

          socket.emit('accept_connect', {
            color: joueur_me.color,
            serveur_id: joueur_me.serveur_id,
            server_socket_id: joueur_me.server_socket_id,
            id: joueur_me.id
          });

        }else {
          socket.emit('game_has_started');
        }
      }else {
        socket.emit('serveur_full');
      }
    }else{
      socket.emit('unknown_connect');
    }
  });

  socket.on('start_game', function () {
    serveurs[serveur_me.id].game_started = true;
    var joueurs_current_game = [];
    for(var i in serveurs[serveur_me.id].list_joueurs) {
      joueurs_current_game.push(serveurs[serveur_me.id].list_joueurs[i]);
    }

    socket.emit('conf_start_game', {
      joueurs: joueurs_current_game
    });
  });

  //Gestion du jeu ----------------------------------------------------

  socket.on('jump', function (jump) {
    socket.broadcast.to(jump.serveur_socket_id).emit('jump', {
      id_joueur: jump.id_joueur
    });
  });

  socket.on('tir', function (tir) {
    socket.broadcast.to(tir.serveur_socket_id).emit('tir', {
      id_joueur: tir.id_joueur
    });
  });

  socket.on('acceleration_mobile', function(acc) {
    socket.broadcast.to(acc.serveur_socket_id).emit('acceleration', {
      x: acc.x,
      y: acc.y,
      id_serveur: acc.id_serveur,
      id_joueur: acc.id_joueur
    });
  });

  //Déconnection ------------------------------------------------------

  socket.on('disconnect', function() {

    if(isEmpty(serveur_me) && isEmpty(joueur_me)) {
      return false;
    }

    if(isEmpty(serveur_me)) {
      //C'est un joueur qui s'est déconnecté
      couleurs.push(joueur_me.color);
      console.log(couleurs);
      if(serveurs[joueur_me.serveur_id]) {
        delete serveurs[joueur_me.serveur_id].list_joueurs[joueur_me.id];
        serveurs[joueur_me.serveur_id].connected_joueur --;
        io.sockets.emit('delete_connected_joueur', {
          serveur_id: serveurs[joueur_me.serveur_id].id,
          joueur_id: joueur_me.id
        });
      }
      console.log('User disconnected');
    }else {
      //C'est un serveur qui s'est déconnecté
      io.sockets.emit('server_disconnected', {
        serveur_id: serveur_me.id
      });
      delete serveurs[serveur_me.id];
      console.log('Server disconnected');
    }

  });

  //Fonctions ---------------------------------------------------------

  function getRandomNumber(min, max) {
    return Math.round(Math.random() * (max - min) + min); //nombre random entre 100'000 et 999'999
  }

  function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  function isEmpty(obj) {
    for(var prop in obj) {
      if(obj.hasOwnProperty(prop))
      return false;
    }

    return true;
  }
});
